import logo from './logo.svg';
import './App.css';
import { useState } from 'react';

function App() {
  const [Num, setNum] = useState(0);
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <button onClick={() => {setNum(Num + 1)}}>hallo</button>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          {Num}
        </a>
      </header>
    </div>
  );
}

export default App;
