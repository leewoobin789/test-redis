// HealthCheckResonse
interface FEHealthCheckResponse {
  status: "pass"|"fail"|"warn";
  version: string;
  notes?: Array<string>;
  description: string;
}

import express from 'express';
import path from 'path'
import bodyParser from 'body-parser';
import cypress from 'cypress'
const app = express();
const port = 3080;
import {Redis} from './config/healthCache';

app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, './app/build')));

//Frondend
app.get('/', (req: express.Request ,res: express.Response) => {
  res.sendFile(path.join(__dirname, './app/build/index.html'));
});

//Healthcheck Endpoint
app.get('/health', async (req: express.Request ,res: express.Response) => {
  //TODO: Redis to get Timestamp and response of last execution &
  //      save current Timestamp asap to avoid any further execution
  //      due to big memory usage
  let ss = await Redis.set("hallo", "hahaha")
  if (ss) {
    Redis.get("hallo", (err,rep) => {
      console.log(err)
      console.log(rep)
    })
  }
  
  if (req.headers["x-api-key"] as string != "testapikey") {
    res.status(401)
    res.json({status:"unmatched"})
    return
  }

  let args: Array<string> = ['cypress', 'run', '--quiet'] // run quite generating few logs
  let options = await cypress.cli.parseRunArguments(args)

  let cyres: CypressCommandLine.CypressRunResult | CypressCommandLine.CypressFailedRunResult = await cypress.run(options)

  if (cyres.status != "finished") {
    res.status(500)
    res.json({status: "fail"})
    return
  }
  res.json({status: "pass"})
});

app.listen(port, () => {
    console.log(`Server listening on the port::${port}`);
});