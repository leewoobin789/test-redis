"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const path_1 = __importDefault(require("path"));
const body_parser_1 = __importDefault(require("body-parser"));
const cypress_1 = __importDefault(require("cypress"));
const app = express_1.default();
const port = 3080;
app.use(body_parser_1.default.json());
app.use(express_1.default.static(path_1.default.join(__dirname, './app/build')));
//Frondend
app.get('/', (req, res) => {
    res.sendFile(path_1.default.join(__dirname, './app/build/index.html'));
});
//Healthcheck Endpoint
app.get('/health', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    if (req.headers["x-api-key"] != "testapikey") {
        res.status(401);
        res.json({ status: "unmatched" });
        return;
    }
    let args = ['cypress', 'run', '--quiet']; // run quite generating few logs
    let options = yield cypress_1.default.cli.parseRunArguments(args);
    let cyres = yield cypress_1.default.run(options);
    if (cyres.status != "finished") {
        res.status(500);
        res.json({ status: "failed" });
        return;
    }
    res.json({ status: "success" });
}));
app.listen(port, () => {
    console.log(`Server listening on the port::${port}`);
});
