var RedisClustr = require("redis-clustr") // No Types exists
import RedisClient from "redis";

export let NewRedis = () => {
    let redis = new RedisClustr({
        servers: [
            {
                host: "cypress.qqpsf5.ng.0001.euc1.cache.amazonaws.com",
                port: 6379
            }
        ],
        createClient: (port: number, host: string) => {
            // this is the default behaviour
            return RedisClient.createClient(port, host);
        }
    });
    
    //connect to redis
    redis.on("connect", function () {
        console.log("connected");
    });

    return redis
}
let redis = NewRedis();

export const Redis = redis;